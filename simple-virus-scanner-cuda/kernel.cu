#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <queue>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <iterator>
#include <ctime>
#include "timer.h"

using namespace std;

static void CheckCudaErrorAux(const char *, unsigned, const char *,
	cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

static int GRID_SIZE;
static int BLOCK_SIZE;

static int NUM_STATES;
static int NUM_STATES_SIZE;

static int FILE_LENGTH;
static int FILE_SIZE;

static int NUM_SIGNATURES;
static int NUM_SIGNATURES_SIZE;

const static int NUM_CHARS = 256;
const static int NUM_CHARS_SIZE = NUM_CHARS * sizeof(int);

struct Signature {
	unsigned char *bytes;
	unsigned int length;
};

void h_print_signatures(Signature *h_signatures) {
	for (int i = 0; i < NUM_SIGNATURES; i++) {
		Signature signature = h_signatures[i];
		for (int j = 0; j < signature.length; j++) {
			printf("%02X ", signature.bytes[j]);
		}
		cout << endl;
	}
}

unsigned char* h_read_file_bytes(const char *h_file_name)
{
	ifstream fl(h_file_name, std::ios::binary);
	fl.seekg(0, ios::end);
	FILE_LENGTH = fl.tellg();
	FILE_SIZE = FILE_LENGTH * sizeof(unsigned char);
	unsigned char *ret = new unsigned char[FILE_LENGTH];
	fl.seekg(0, ios::beg);
	fl.read(reinterpret_cast<char*>(ret), FILE_LENGTH);
	fl.close();
	return ret;
}


vector<unsigned char> h_read_file(const char* h_file_name)
{
	std::ifstream file(h_file_name, std::ios::binary);

	file.unsetf(std::ios::skipws);

	std::streampos fileSize;

	file.seekg(0, std::ios::end);
	fileSize = file.tellg();
	FILE_LENGTH = fileSize;
	FILE_SIZE = FILE_LENGTH * sizeof(char);
	file.seekg(0, std::ios::beg);

	std::vector<unsigned char> vec;
	vec.reserve(fileSize);

	vec.insert(vec.begin(),
		std::istream_iterator<unsigned char>(file),
		std::istream_iterator<unsigned char>());

	return vec;
}

vector<vector<unsigned char>> h_read_virus_definitions(const char *h_virus_def_fname) {

	std::ifstream file(h_virus_def_fname);
	vector<vector<unsigned char>> signatures;
	string line;
	int count_sig = 0;

	while (getline(file, line)) {
		vector<unsigned char> bytes;

		std::istringstream hex_chars_stream(line);
	    int count_bytes = 0;
		unsigned int c;

		while (hex_chars_stream >> std::uppercase >> std::hex >> c) {
			bytes.push_back(c);
			NUM_STATES++;
			count_bytes++;
		}

		signatures.push_back(bytes);
		count_sig++;
	}
	file.close();

	NUM_STATES++;
	NUM_SIGNATURES = count_sig;
	NUM_SIGNATURES_SIZE = NUM_SIGNATURES * sizeof(Signature);
	NUM_STATES_SIZE = NUM_STATES * sizeof(int);
	return signatures;
}

// AHO-CORASICK ALGORITHM //

__host__ int h_buildMatchingMachine(int *h_out, int *h_f, int *h_g, Signature *h_signatures)
{
	int states = 1;

	for (int i = 0; i < NUM_SIGNATURES; ++i)
	{
		const unsigned char *signature = h_signatures[i].bytes;
		const unsigned int signature_len = h_signatures[i].length;
		int currentState = 0;

		for (unsigned int j = 0; j < signature_len; ++j)
		{

			unsigned char ch = signature[j];

			if (h_g[currentState * NUM_CHARS + ch] == -1)
				h_g[currentState * NUM_CHARS + ch] = states++;

			currentState = h_g[currentState * NUM_CHARS + ch]; 
		}

		h_out[currentState] |= (1 << i);
	}

	for (int ch = 0; ch < NUM_CHARS; ++ch)
		if (h_g[ch] == -1)
			h_g[ch] = 0;

	queue<int> q;

	for (int ch = 0; ch < NUM_CHARS; ++ch)
	{
	
		if (h_g[ch] != 0)
		{
			h_f[h_g[ch]] = 0;
			q.push(h_g[ch]);
		}
	}

	while (q.size())
	{
		int state = q.front();
		q.pop();

		for (int ch = 0; ch < NUM_CHARS; ++ch)
		{
			if (h_g[state * NUM_CHARS + ch] != -1)
			{
				int failure = h_f[state];

				while (h_g[failure * NUM_CHARS + ch] == -1)
					failure = h_f[failure];

				failure = h_g[failure * NUM_CHARS + ch];
				h_f[h_g[state * NUM_CHARS + ch]] = failure;

				h_out[h_g[state * NUM_CHARS + ch]] |= h_out[failure];

				q.push(h_g[state * NUM_CHARS + ch]);
			}
		}
	}

	return states;
}


__device__ __host__ int h_d_findNextState(const int * __restrict__ h_f, const int * __restrict__ h_g, int currentState, unsigned char nextInput)
{
	int answer = currentState;

	while (h_g[answer * NUM_CHARS + nextInput] == -1)
		answer = h_f[answer];

	return h_g[answer * NUM_CHARS + nextInput];
}


__host__ void h_searchSignatures(const int *h_out, const int *h_f, const int *h_g, const Signature *h_signatures, const unsigned char *h_file)
{
	int currentState = 0;

	for (int i = 0; i < FILE_LENGTH; ++i)
	{
		unsigned char nextInput = h_file[i];
		currentState = h_d_findNextState(h_f, h_g, currentState, nextInput);

		if (h_out[currentState] == 0)
			continue;

		for (int j = 0; j < NUM_SIGNATURES; ++j)
		{
			if (h_out[currentState] & (1 << j))
			{
				cout << "Signature " << j << " found!" << endl;
			}
		}
	}
}

__global__ void d_scan_file(const int * __restrict__ d_out, const int * __restrict__ d_f, const int * __restrict__ d_g, const unsigned char * __restrict__ d_file, int * d_count_found,
	int NUM_CHARS, int NUM_SIGNATURES, int NUM_SEARCHES, int NUM_STATES) {

	extern __shared__ int shared_mem[];

	int *s_out = (int*)&shared_mem;
	int *s_f = (int*)&shared_mem[NUM_STATES];

	if (threadIdx.x == 0) {
		for (int i = 0; i < NUM_STATES; i++) {
			s_out[i] = d_out[i];
			s_f[i] = d_f[i];
		}
	}
	__syncthreads();

	int scan_from = (blockIdx.x * blockDim.x + threadIdx.x) * NUM_SEARCHES;
	int scan_to = scan_from + NUM_SEARCHES;
	//printf("Reading from %d to %d\n", scan_from, scan_to);

	int currentState = 0;
	unsigned char nextInput;

	for (int i = scan_from; i < scan_to; ++i)
	{
		nextInput = d_file[i];

		currentState = h_d_findNextState(s_f, d_g, currentState, nextInput);

		if (s_out[currentState] == 0)
			continue;
		
		for (int j = 0; j < NUM_SIGNATURES; ++j)
		{
			if (s_out[currentState] & (1 << j))
			{
				printf("Signature %d found!\n", j);
				//atomicAdd(d_count_found, 1);
			}
		} 
	} 
}

int main(int argc, char **argv)
{
	char *h_file_name, *h_virus_def_fname;
	if (argc == 3) {
		h_virus_def_fname = argv[1];
		h_file_name = argv[2];
	}
	else {
		cout << "Invalid usage, 2 arguments (virus def and file) expected. Exiting!" << endl;
		exit(EXIT_FAILURE);
	}

	unsigned char *h_file;
	vector<vector<unsigned char>> h_v_signatures;
	h_file = h_read_file_bytes(h_file_name);
	h_v_signatures = h_read_virus_definitions(h_virus_def_fname);

	Signature *h_signatures = new Signature[h_v_signatures.size()];
	for (int i = 0; i < h_v_signatures.size(); i++) {
		h_signatures[i].bytes = new unsigned char[h_v_signatures[i].size()];
		h_signatures[i].length = h_v_signatures[i].size();
		copy(h_v_signatures[i].begin(), h_v_signatures[i].end(), h_signatures[i].bytes);
	}
	h_v_signatures.clear();
	h_print_signatures(h_signatures);
	cout << endl;
	
    int *h_out = new int[NUM_STATES];
	int *h_f = new int[NUM_STATES];
	int *h_g = new int[NUM_STATES * NUM_CHARS];
	int *h_count_found = new int[1];
	h_count_found[0] = 0;

	memset(h_out, 0, NUM_STATES * sizeof(int));
	memset(h_f, -1, NUM_STATES * sizeof(int));
	memset(h_g, -1, NUM_STATES * NUM_CHARS * sizeof(int));

	h_buildMatchingMachine(h_out, h_f, h_g, h_signatures);

	std::clock_t time_cpu;
	time_cpu = std::clock();
	h_searchSignatures(h_out, h_f, h_g, h_signatures, h_file);
	double ms_cpu = (std::clock() - time_cpu) / (double)(CLOCKS_PER_SEC / 1000);
	cout << endl << "CPU TIME: " << ms_cpu << " ms" << endl;

	//Signature *d_signatures;
	unsigned char *d_file;
	int *d_out;
	int *d_f;
	int *d_g;
	int *d_count_found;

	//cudaMalloc((void**)&d_signatures, NUM_SIGNATURES_SIZE);
	cudaMalloc((void**)&d_file, FILE_SIZE);
	cudaMalloc((void**)&d_out, NUM_STATES_SIZE);
	cudaMalloc((void**)&d_f, NUM_STATES_SIZE);
	cudaMalloc((void**)&d_g, NUM_STATES * NUM_CHARS * sizeof(int));
	cudaMalloc((void**)&d_count_found, sizeof(int));

	//cudaMemcpy(d_signatures, h_signatures, NUM_SIGNATURES_SIZE, cudaMemcpyHostToDevice);
	cudaMemcpy(d_file, h_file, FILE_SIZE, cudaMemcpyHostToDevice);
	cudaMemcpy(d_out, h_out, NUM_STATES_SIZE, cudaMemcpyHostToDevice);
	cudaMemcpy(d_f, h_f, NUM_STATES_SIZE, cudaMemcpyHostToDevice);
	cudaMemcpy(d_g, h_g, NUM_STATES * NUM_CHARS * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_count_found, h_count_found, sizeof(int), cudaMemcpyHostToDevice);

	/* 
	int numSMs;
	cudaDeviceGetAttribute(&numSMs, cudaDevAttrMultiProcessorCount, 0); 
	*/


	BLOCK_SIZE = 1024;
	GRID_SIZE = 512;
	int NUM_SEARCHES = FILE_LENGTH / (BLOCK_SIZE * GRID_SIZE);
	int SHARED_MEM_SIZE = NUM_STATES * 2 * sizeof(int);
	dim3 BLOCK_SIZE_(BLOCK_SIZE);
	dim3 GRID_SIZE_(GRID_SIZE);

	GpuTimer gpuTimer;
	gpuTimer.Start();
	d_scan_file << <GRID_SIZE_, BLOCK_SIZE_, SHARED_MEM_SIZE>> > (d_out, d_f, d_g, d_file, d_count_found, 
		NUM_CHARS, NUM_SIGNATURES, NUM_SEARCHES, NUM_STATES);

	gpuTimer.Stop();
	cout << endl << "GPU TIME: " << gpuTimer.Elapsed() << " ms" << endl;

	//cudaMemcpy(h_count_found, d_count_found, sizeof(int), cudaMemcpyDeviceToHost);

	//cout << endl;
	//cout << "Found: " << h_count_found[0] << endl;

	delete[] h_file;
	for (int i = 0; i < NUM_SIGNATURES; i++) {
		delete[] h_signatures[i].bytes;
	}
	delete[] h_signatures;
	delete[] h_out;
	delete[] h_f;
	delete[] h_g;

	cudaFree(d_file);
	cudaFree(d_out);
	cudaFree(d_f);
	cudaFree(d_g);
	cudaFree(d_count_found);

	cudaDeviceSynchronize();

	return 0;
}

static void CheckCudaErrorAux(const char *file, unsigned line,
	const char *statement, cudaError_t err) {
	if (err == cudaSuccess)
		return;
	std::cerr << statement << " returned " << cudaGetErrorString(err) << "("
		<< err << ") at " << file << ":" << line << std::endl;
	exit(1);
}